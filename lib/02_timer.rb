class Timer
  # TODO: your code goes here!

  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    hours = @seconds / 3600
    hours < 10 ? hours_disp = "0#{hours}" : hours_disp = hours.to_s

    min = (@seconds / 60) % 60
    min < 10 ? min_disp = "0#{min}" : min_disp = min.to_s

    sec = @seconds - (3600 * hours) - (60 * min)
    sec < 10 ? sec_disp = "0#{sec}" : sec_disp = sec.to_s

    "#{hours_disp}:#{min_disp}:#{sec_disp}"
  end
end
