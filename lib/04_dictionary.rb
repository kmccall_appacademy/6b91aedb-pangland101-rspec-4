class Dictionary
  # TODO: your code goes here!

  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def keywords
    @d.keys.sort
  end

  def include?(word)
    self.keywords.include?(word) ? true : false
  end

  def find(word)
    @d.select { |key, _val| key[0...word.length] == word }
  end

  def printable
    out = ""
    self.keywords.each { |word| out += "[#{word}] \"#{@d[word]}\"\n" }
    out.chomp
  end

  def add(entry)
    if entry.to_s == entry
      @d[entry] = nil
    else
      @d[entry.keys[0]] = entry.values[0]
    end
  end
end
