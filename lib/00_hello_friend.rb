class Friend
  # TODO: your code goes here!
  def greeting(who = nil)
    # "I thought the Godfather was only OK"
    return "Hello!" if who.nil?
    "Hello, #{who}!"
  end
end
