class Temperature
  # TODO: your code goes here!

  def initialize(options_hash)
    @metric = options_hash.keys[0]
    @temp = options_hash.values[0]
  end

  def in_fahrenheit
    @metric == :f ? @temp : @temp * 9.0 / 5 + 32
  end

  def in_celsius
    @metric == :f ? (@temp - 32) * 5.0 / 9 : @temp
  end

  def self.from_celsius(temperature)
    Temperature.new(:c => temperature)
  end

  def self.from_fahrenheit(temperature)
    Temperature.new(:f => temperature)
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temp = temp
    @metric = :c
  end

  def in_celsius
    super
  end

  def in_fahrenheit
    super
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temp = temp
    @metric = :f
  end

  def in_celsius
    super
  end

  def in_fahrenheit
    super
  end
end
