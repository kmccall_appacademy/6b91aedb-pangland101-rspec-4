class Book
  # TODO: your code goes here!
  attr_reader :title

  def title=(title)
    @title = titlefy(title)
  end

  def titlefy(string)
    title_split = string.split(" ")
    off_limits = ["the", "and", "in", "a", "an", "of"]
    title_split[0][0] = title_split[0][0].upcase
    title_split.map do |word|
      off_limits.include?(word) ? word : word[0].upcase + word[1..-1]
    end.join(" ")
  end
end
